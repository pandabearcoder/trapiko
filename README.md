### Traffic App for Philippine Metro ways

Created with React Native, Nativebase

This project is possible with the Python Wrapper created by a good friend
which can be found on [MMDA Traffic API Wrapper](https://github.com/drfb/mmda-traffic-api)

Currently, this project is being inactive due to the inconsistency of the
availability of MMDA's API
