import React, { Component } from 'react';
import {
  Card,
  CardItem,
  Text,
} from 'native-base';

class Segment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      traffic: {},
    };
  }

  static navigationOptions = ({navigation}) => ({
    title: navigation.state.params.name,
  });

  render() {
    const { params } = this.props.navigation.state;
    return (
      <Card>
        <CardItem>
          <Text>{params.title}</Text>
          <Text>{params.status}</Text>
          <Text>{params.date}</Text>
        </CardItem>
      </Card>
    );
  }
}

export default Segment;
