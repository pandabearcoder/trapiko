import React from 'react';
import {
  Badge,
  Text
} from 'native-base';

function TrafficIndicator({trafficStatus}) {
  switch(trafficStatus)  {
    case 'Light':
    case 'Light to Moderate':
      return (
        <Badge success>
          <Text>{trafficStatus.substring(0, 1)}</Text>
        </Badge>
      );
      break;
    case 'Moderate':
    case 'Moderate to Heavy':
      return (
        <Badge warning>
          <Text>{trafficStatus.substring(0, 1)}</Text>
        </Badge>
      );
      break;
    case 'Heavy':
      return (
        <Badge error>
          <Text>{trafficStatus.substring(0, 1)}</Text>
        </Badge>
      );
      break;
  }
}

export default TrafficIndicator;
