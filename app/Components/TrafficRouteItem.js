import React from 'react';
import {
  CardItem,
  Text,
  H3,
  Left,
  Body,
} from 'native-base';
import TrafficIndicator from './TrafficIndicator';

function TrafficRouteItem(props) {
  return (
    <CardItem>
      <Left>
        <TrafficIndicator trafficStatus={props.trafficStatus} />
        <Body>
          <Text>{props.name}</Text>
          <Text note>{props.date}</Text>
        </Body>
      </Left>
    </CardItem>
  );
}

export default TrafficRouteItem;
