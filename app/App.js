import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';

import TrafficRouteList from './Containers/TrafficRouteList';
import MainRouter from './Routers/MainRouter';

class App extends Component {
  render() {
    return <MainRouter />;
  }
}

export default App;
