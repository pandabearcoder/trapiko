const APP_NAME = 'Trapiko';
const API_SERVER = '0.0.0.0:8000';

export {
  APP_NAME,
  API_SERVER,
};
