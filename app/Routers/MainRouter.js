import { StackNavigator } from 'react-navigation';
import TrafficRouteList from 'app/Containers/TrafficRouteList';
import Segment from 'app/Containers/Segment';

const MainRouter = StackNavigator({
    Main: {screen: TrafficRouteList},
    Segment: {screen: Segment},
});

export default MainRouter;
