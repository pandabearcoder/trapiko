import React, { Component } from 'react';
import {
  Container,
  Header,
  Left,
  Title,
  Button,
  Icon,
  Body,
  Right,
  Content,
  Text,
  Card,
} from 'native-base';
import TrafficRouteItem from 'app/Components/TrafficRouteItem';
import {
  API_SERVER,
} from 'app/Constants';

class TrafficRouteList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      traffic: {},
      isUpdating: true,
    };
  }

  static navigationOptions = {
    title: 'Highway',
  };

  componentDidMount() {
    let endpoint = '/v1/highways/EDSA/traffic';
    let url = (
      API_SERVER +
      endpoint
    );
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isUpdating: false,
          traffic: responseJson,
        });
      })
      .catch((exception) => {
        console.log('ERROR: ' + exception);
      });
  }

  render() {
    if (this.state.isUpdating) {
      return <Loading />;
    }

    const {navigate} = this.props.navigation;
    let routeItems = this.state.traffic.segments.map((segment) => {
      return (
        <TrafficRouteItem
          key={segment.id}
          name={segment.label}
          trafficStatus={segment.traffic.NB.status}
          date={segment.traffic.NB.updated_at}
          onPress={() => navigate(
            'Segment',
            {
              name: segment.label,
              status: segment.traffic.NB.status,
              date: segment.traffic.NB.updated_at
            })}
        />
      )
    });

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Trapiko</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Card>
            {routeItems}
          </Card>
        </Content>
      </Container>
    );
  }
}

function Loading() {
  return (
    <Text>Getting latest traffic updates...</Text>
  );
}

export default TrafficRouteList;
